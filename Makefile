CFLAGS= -Wall -Wextra -O2
LDFLAGS= -lm

all: hilbert.zip truchet.zip

clean:
	rm *.pgm

truchet.zip: truchet.gif
	./frameit truchet

truchet.gif: truchet
	./truchet
	./animate truchet

hilbert.zip: hilbert.gif
	./frameit hilbert

hilbert.gif: hilbert
	./n-times 180 ./hilbert
	./animate hilbert
